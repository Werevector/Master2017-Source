// Fill out your copyright notice in the Description page of Project Settings.
using System.IO;
using UnrealBuildTool;

public class ParametricArt18 : ModuleRules
{
    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }
    }
    public ParametricArt18(ReadOnlyTargetRules Target) : base(Target)
	{

		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        //PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "GaussianLib", "include"));
        //PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "GeometronLib", "include"));
       // PrivateIncludePaths.Add(Path.Combine(ThirdPartyPath, "GeometronLib", "sources"));
        //PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "GeometronLib", "geomlib.lib"));

        //PublicIncludePaths.Add("C:/programming/unrealprojects/18/ParametricArt18/ThirdParty/GaussianLib/include");
        //PublicSystemIncludePaths.Add("C:/programming/unrealprojects/18/ParametricArt18/ThirdParty/GaussianLib/include");
        //PublicIncludePaths.Add("C:/programming/unrealprojects/18/ParametricArt18/ThirdParty/GeometronLib/include");
        //PublicSystemIncludePaths.Add("C:/programming/unrealprojects/18/ParametricArt18/ThirdParty/GeometronLib/include");

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });


		PrivateDependencyModuleNames.AddRange(new string[] {  });
		
		PublicDependencyModuleNames.AddRange(new string[] { "ShaderCore", "RenderCore", "RHI", "RuntimeMeshComponent" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
