// Fill out your copyright notice in the Description page of Project Settings.

#include "Composer_Scene.h"
#include "ParallelFor.h"

// Sets default values
AComposer_Scene::AComposer_Scene()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RMC = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("Runtime Mesh"));
	HISMC = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("InstanceMeshHierarcical"));
	//RootComponent = RMC;
}

// Called when the game starts or when spawned
void AComposer_Scene::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AComposer_Scene::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//----------------
// POINT FUNCTIONS
//----------------
TArray<FVector> AComposer_Scene::Circle(float resolution, float r, FVector center)
{
	TArray<FVector> result;
	if (resolution < 1.0f)
		return result;
	float x = 0.0f;
	float rad = (360*(3.14 / 180.0));
	float theta = rad / resolution;
	
	while(x < rad)
	{
		float y1 = r*sin(x);
		float y2 = r*cos(x);
		x += theta;
		result.Add(center + FVector(y1,y2, 0.0f));
	}
	return result;
}

TArray<FVector> AComposer_Scene::Ray(float resolution, FVector origin, FVector direction)
{
	TArray<FVector> result;
	float t = 0.0f;
	float theta = 1.0f / resolution;
	while(t < 1.0f)
	{
		result.Add(origin + direction*t);
		t += theta;
	}
	return result;
}

TArray<FVector> AComposer_Scene::Square(float resolution, FVector diagonal)
{
	TArray<FVector> result;
	if (resolution < 1.0f)
		return result;

	float t = 0.0f;
	float theta = 1.0f / (resolution);
	FVector half = diagonal / 2;
	TArray<FVector> r1, r2, r3 ,r4;
	while(t < 1.0f)											
	{
		r1.Add(FVector(t*diagonal.X, 0, t/2*diagonal.Z) - half);
		r2.Add(FVector(0, t*diagonal.Y, t/2*diagonal.Z) - half);
		r3.Add(FVector(diagonal.X, t*diagonal.Y, t/2*diagonal.Z+half.Z) - half);
		r4.Add(FVector(t*diagonal.X, diagonal.Y, t/2*diagonal.Z+half.Z) - half);
		t += theta;
	}
	r4.Add(diagonal - half);
	result.Append(r1);
	result.Append(r2);
	result.Append(r3);
	result.Append(r4);
	return result;
}

TArray<FVector> AComposer_Scene::Spiral(float resolution, float radius, float height, int revolutions)
{
	TArray<FVector> result;

	float i = 0.0f;
	float rad = (360*revolutions*(3.14 / 180.0));
	float theta = rad / resolution;

	while (i < rad)
	{
		float A = sin( (i/rad)*3.14 );
		float x = A*radius*cos(i);
		float y = A*radius*sin(i);
		float z = (i/rad)*height;
		i += theta;
		result.Add(FVector(x, y, z));
	}

	return result;
}

TArray<FVector> AComposer_Scene::Wave(FVector direction, bool X, bool Y, bool Z, float resolution, float amplitude, float frequency)
{
	TArray<FVector> result;
	float t = 0.0f;
	float theta = 1.0f / resolution;
	while(t < 1.0f)
	{
	    float x = t * direction.X;
		float y = t * direction.Y;
		float z = t * direction.Z;
		if(X)
			x += amplitude * cos(t*frequency);
		if(Y)
			y += amplitude * sin(t*frequency);
		if(Z)
			z += amplitude * cos(t*frequency);
		result.Add(FVector(x,y,z));
		t += theta;
	}
	return result;
}

TArray<FVector> AComposer_Scene::Noise(const FVector point, const int count, const float width, const float length, const float height)
{
	TArray<FVector> result;

	std::default_random_engine generator;
	std::uniform_real_distribution<float> widthDistribution(-width, width);
	std::uniform_real_distribution<float> lengthDistribution(-length, length);
	std::uniform_real_distribution<float> heightDistribution(-height, height);

	for (size_t i = 0; i < count; i++)
	{
		FVector npoint = point;
		if (width != 0.0f)
			npoint.X += widthDistribution(generator);
		if (length != 0.0f)
			npoint.Y += lengthDistribution(generator);
		if (height != 0.0f)
			npoint.Z += heightDistribution(generator);
		result.Add(npoint);
	}

	return result;
}

FBox AComposer_Scene::SectionBoundingBox(int section)
{
	FBox resultBox;
	RMC->GetSectionBoundingBox(section, resultBox);
	return resultBox;
}

FBox AComposer_Scene::BoundingBox(FMeshData mesh)
{
	FBox resultBox;
	auto box = mesh.GmMesh.BoundingBox();
	resultBox.Max = GsVector3ToFVector(box.max);
	resultBox.Min = GsVector3ToFVector(box.min);
	return resultBox;
}

void AComposer_Scene::CenterAndExtent(FMeshData mesh, FVector & center, FVector & extent)
{
	auto box = mesh.GmMesh.BoundingBox();
	center = GsVector3ToFVector(box.Center());
	extent = GsVector3ToFVector(box.Size());
}

void AComposer_Scene::Clipped(FMeshData mesh, FVector p1, FVector p2, FVector p3, bool winding, FMeshData& front, FMeshData& back)
{
	Gm::TriangleMesh tFront;
	Gm::TriangleMesh tBack;
	Gm::Plane plane;
	plane.Build(
	{ p1.X, p1.Y, p1.Z },
	{ p2.X, p2.Y, p2.Z },
	{ p3.X, p3.Y, p3.Z });
	Gm::MeshModifier::ClipMesh(mesh.GmMesh, plane, tFront, tBack);
	
	front.GmMesh = tFront;
	back.GmMesh = tBack;
	
	CopyGmTriangleMeshToBuffers(front);
	CopyGmTriangleMeshToBuffers(back);
}

void AComposer_Scene::Transformed(FTransform transform, FMeshData mesh, FMeshData & outMesh)
{
	outMesh.GmMesh = TransformGmMesh(transform, mesh.GmMesh);
	CopyGmTriangleMeshToBuffers(outMesh);
}

FMeshData AComposer_Scene::MakeMeshData(TArray<FVector> vertices, TArray<FVector> normals, TArray<FVector2D> texturecoordinates, TArray<int> triangles)
{
	FMeshData result;
	Gm::TriangleMesh mesh;
	for (size_t i = 0; i < vertices.Num(); i++)
	{
		mesh.AddVertex(
			Gs::Vector3(vertices[i].X, vertices[i].Y, vertices[i].Z),
			Gs::Vector3(normals[i].X, normals[i].Y, normals[i].Z),
			Gs::Vector2(texturecoordinates[i].X, texturecoordinates[i].Y)
		);
	}
	for (size_t i = 0; i < triangles.Num() - 4; i += 3)
	{
		mesh.AddTriangle(triangles[i], triangles[i+1], triangles[i+2]);
	}

	result.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(result);
	return result;
}

void AComposer_Scene::Staple(FMeshData mesh, FMeshData staple, bool left, bool right, bool top, bool bottom, bool front, bool back, FMeshData& outMesh)
{
	//If target mesh empty, simply copy the staple mesh
	if (mesh.GmMesh.vertices.size() <= 0)
	{
		outMesh = staple;
		return;
	}

	FVector center;
	FVector size;

	FVector stCenter;
	FVector stSize;

	CenterAndExtent(mesh, center, size);
	CenterAndExtent(staple, stCenter, stSize);

	size /= 2;
	stSize /= 2;

	if (left)
	{
		CopyGmMeshToPoint(FTransform(FRotator::ZeroRotator, FVector(center.X + size.X - stCenter.X + stSize.X, 0.0, 0.0), FVector(1.0, 1.0, 1.0)), staple.GmMesh, mesh.GmMesh);
	}
	if (right)
	{
		CopyGmMeshToPoint(FTransform(FRotator::ZeroRotator, FVector(center.X - size.X - stCenter.X - stSize.X, 0.0, 0.0), FVector(1.0, 1.0, 1.0)), staple.GmMesh, mesh.GmMesh);
	}
	if (top)
	{
		CopyGmMeshToPoint(FTransform(FRotator::ZeroRotator, FVector(0.0, 0.0, center.Z + size.Z - stCenter.Z + stSize.Z), FVector(1.0, 1.0, 1.0)), staple.GmMesh, mesh.GmMesh);
	}
	if (bottom)
	{
		CopyGmMeshToPoint(FTransform(FRotator::ZeroRotator, FVector(0.0, 0.0, center.Z - size.Z - stCenter.Z - stSize.Z), FVector(1.0, 1.0, 1.0)), staple.GmMesh, mesh.GmMesh);
	}
	if (front) 
	{
		CopyGmMeshToPoint(FTransform(FRotator::ZeroRotator, FVector(0.0, center.Y + size.Y - stCenter.Y + stSize.Y, 0.0), FVector(1.0, 1.0, 1.0)), staple.GmMesh, mesh.GmMesh);
	}
	if (back) 
	{
		CopyGmMeshToPoint(FTransform(FRotator::ZeroRotator, FVector(0.0, center.Y - size.Y - stCenter.Y - stSize.Y, 0.0), FVector(1.0, 1.0, 1.0)), staple.GmMesh, mesh.GmMesh);
	}

	int x = left + right + top + bottom + front + back;
	for (size_t i = 0; i < x; i++)
	{
		mesh.color.Append(staple.color);
	}
	outMesh.color = mesh.color;

	outMesh.GmMesh = mesh.GmMesh;
	CopyGmTriangleMeshToBuffers(outMesh);
}



void AComposer_Scene::SectionData(int index, FMeshData & outMesh)
{
	TArray<FVector>* _Positions;
	TArray<FRuntimeMeshVertexNoPosition>* _Vertices;
	TArray<int32>* _Triangles;

	RMC->BeginMeshSectionUpdate(index, _Positions, _Vertices, _Triangles);
	outMesh = ConvertToGMMesh(*_Positions, *_Vertices, *_Triangles);
}

void AComposer_Scene::TriangleCount(int index, int & count)
{
	TArray<FVector>* _Positions;
	TArray<FRuntimeMeshVertexNoPosition>* _Vertices;
	TArray<int32>* _Triangles;
	RMC->BeginMeshSectionUpdate(index, _Positions, _Vertices, _Triangles);
	count = _Triangles->Num();
}

void AComposer_Scene::Snap(FMeshData target, FMeshData mesh, EFacingEnum facing, FMeshData & outMesh)
{
	if (target.GmMesh.vertices.size() <= 0)
	{
		outMesh = mesh;
		return;
	}

	FVector center;
	FVector size;

	FVector tCenter;
	FVector tSize;

	CenterAndExtent(target, tCenter, tSize);
	CenterAndExtent(mesh, center, size);

	size /= 2;
	tSize /= 2;

	FTransform targetTransform;

	switch (facing)
	{
	case EFacingEnum::FE_Left:
		targetTransform = FTransform(FRotator::ZeroRotator, FVector(tCenter.X + tSize.X - center.X + size.X, 0.0, 0.0));
		break;
	case EFacingEnum::FE_Right:
		targetTransform = FTransform(FRotator::ZeroRotator, FVector(tCenter.X - tSize.X - center.X - size.X, 0.0, 0.0));
		break;
	case EFacingEnum::FE_Top:
		targetTransform = FTransform(FRotator::ZeroRotator, FVector(0.0, 0.0, tCenter.Z + tSize.Z - center.Z + size.Z));
		break;
	case EFacingEnum::FE_Bottom:
		targetTransform = FTransform(FRotator::ZeroRotator, FVector(0.0, 0.0, tCenter.Z - tSize.Z - center.Z - size.Z));
		break;
	case EFacingEnum::FE_Front:
		targetTransform = FTransform(FRotator::ZeroRotator, FVector(0.0, tCenter.Y + tSize.Y - center.Y + size.Y, 0.0));
		break;
	case EFacingEnum::FE_Back:
		targetTransform = FTransform(FRotator::ZeroRotator, FVector(0.0, tCenter.Y - tSize.Y - center.Y - size.Y, 0.0));
		break;
	default:
		break;
	}

	outMesh.GmMesh = TransformGmMesh(targetTransform, mesh.GmMesh);
	outMesh.color = mesh.color;
	CopyGmTriangleMeshToBuffers(outMesh);
}

void AComposer_Scene::Colored(FMeshData mesh, FColor color, FMeshData& outMeshData)
{
	for (size_t i = 0; i < mesh.color.Num(); i++)
		mesh.color[i] = color;
	outMeshData = mesh;
}

void AComposer_Scene::Mirrored(FMeshData mesh, FVector point, FMeshData& outMesh)
{
	FVector center;
	FVector size;
	CenterAndExtent(mesh, center, size);

	FVector meshToPointVector = point - center;
	FTransform transform = FTransform( FRotator::ZeroRotator, FVector(meshToPointVector*2), FVector(1.0, 1.0, 1.0));
	CopyGmMeshToPoint(transform, mesh.GmMesh, mesh.GmMesh);
	outMesh.color = mesh.color;
	outMesh.color.Append(mesh.color);
	outMesh.GmMesh = mesh.GmMesh;
	CopyGmTriangleMeshToBuffers(outMesh);
}

void AComposer_Scene::Centered(FMeshData mesh, FVector point, FMeshData & outMesh)
{
	FVector center;
	FVector size;
	CenterAndExtent(mesh, center, size);
	FTransform transform = FTransform( FRotator::ZeroRotator, point - center, FVector(1.0,1.0,1.0) );
	outMesh.GmMesh = TransformGmMesh(transform, mesh.GmMesh);
	outMesh.color = mesh.color;
	CopyGmTriangleMeshToBuffers(outMesh);
}

TArray<FVector> AComposer_Scene::Rotated(TArray<FVector> Points, FRotator rotation)
{
	for (size_t i = 0; i < Points.Num(); i++)
	{
		Points[i] = rotation.RotateVector(Points[i]);
	}
	return Points;
}

void AComposer_Scene::Instantiated(TArray<FVector> points, FTransform transform, bool make_XRotator, bool make_YRotator, bool make_ZRotator)
{
	for (size_t i = 0; i < points.Num(); i++)
	{
		FQuat rot = points[i].ToOrientationQuat();
		if (!make_XRotator)
			rot.X = 0.0f;
		if (!make_YRotator)
			rot.Y = 0.0f;
		if (!make_ZRotator)
			rot.Z = 0.0f;
		rot.Normalize();
		FVector scale(1.0f, 1.0f, 1.0f);


		HISMC->AddInstance(transform * FTransform(rot, points[i], scale));
	}
}

void AComposer_Scene::CopyGmMeshToPoint(FTransform transform, Gm::TriangleMesh inMesh, Gm::TriangleMesh& outMesh)
{
	Gm::TriangleMesh copiedMesh = TransformGmMesh(transform, inMesh);
	outMesh.Append(copiedMesh);
}

void AComposer_Scene::Copied(FMeshData mesh, TArray<FVector> points, bool make_XRotator, bool make_YRotator, bool make_ZRotator, FMeshData& outMeshData)
{
	FVector scale = FVector(1.0, 1.0, 1.0);

	for (size_t i = 0; i < points.Num(); i++)
	{
		FQuat rot = points[i].ToOrientationQuat();
		if (!make_XRotator)
			rot.X = 0.0f;
		if (!make_YRotator)
			rot.Y = 0.0f;
		if (!make_ZRotator)
			rot.Z = 0.0f;
		rot.Normalize();

		CopyGmMeshToPoint(FTransform(rot, points[i], scale), mesh.GmMesh, outMeshData.GmMesh);
		outMeshData.color.Append(mesh.color);
	}
	
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::Merged(FMeshData meshData_1, FMeshData meshData_2, FMeshData& outMeshData)
{
	meshData_1.GmMesh.Append(meshData_2.GmMesh);
	outMeshData.GmMesh = meshData_1.GmMesh;
	
	meshData_1.color.Append(meshData_2.color);
	outMeshData.color = meshData_1.color;
	
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::Repeated_Sec(int section, int Xnr, int Ynr, int Znr)
{
	TArray<FRuntimeMeshVertexNoPosition> Vertices;
	TArray<FRuntimeMeshVertexNoPosition> VerticesX;
	TArray<FRuntimeMeshVertexNoPosition> VerticesY;
	TArray<FVector> Positions;
	TArray<FVector> PositionsX;
	TArray<FVector> PositionsY;
	TArray<int> Indices;
	FBox BoundingBox;

	TArray<FVector>* _Positions;
	TArray<FRuntimeMeshVertexNoPosition>* _Vertices;
	TArray<int32>* _Triangles;
	
	RMC->BeginMeshSectionUpdate(section, _Positions, _Vertices, _Triangles);
	RMC->GetSectionBoundingBox(section, BoundingBox);

	FVector xOff = FVector(BoundingBox.Max.X - BoundingBox.Min.X, 0, 0);
	FVector yOff = FVector(0, BoundingBox.Max.Y - BoundingBox.Min.Y, 0);
	FVector zOff = FVector(0, 0, BoundingBox.Max.Z - BoundingBox.Min.Z);

	for (size_t x = 0; x < Xnr; x++)
	{
		for (size_t i = 0; i < _Vertices->Num(); i++)
		{
			auto vert = (*_Vertices)[i];
			FRuntimeMeshVertexNoPosition vertex;
			vertex.Color = vert.Color;
			vertex.Normal = vert.Normal;
			vertex.Tangent = vert.Tangent;
			vertex.UV0 = vert.UV0;
			
			FVector position = (*_Positions)[i] + xOff*x;
			VerticesX.Push(vertex);
			PositionsX.Push(position);
		}
	}

	for (size_t y = 0; y < Ynr; y++)
	{
		for (size_t i = 0; i < VerticesX.Num(); i++)
		{
			FVector pos = PositionsX[i];
			pos += yOff*y;
			VerticesY.Push(VerticesX[i]);
			PositionsY.Push(pos);
		}
	}

	for (size_t z = 0; z < Znr; z++)
	{
		for (size_t i = 0; i < VerticesY.Num(); i++)
		{
			FVector pos = PositionsY[i];
			pos += zOff*z;
			Vertices.Push(VerticesY[i]);
			Positions.Push(pos);

		}
	}
	
	int number = Xnr * Ynr * Znr;
	for (size_t i = 0; i < (int)number; i++)
	{
		for (size_t j = 0; j < _Triangles->Num(); j++)
		{
			Indices.Add((*_Triangles)[j] + _Vertices->Num()*i);
		}
	}

	RMC->CreateMeshSectionDualBuffer(section, Positions, Vertices, Indices, false, EUpdateFrequency::Frequent);
}

void AComposer_Scene::Repeated(FMeshData mesh, int Xnr, int Ynr, int Znr, FMeshData & outMesh)
{
	FVector center;
	FVector size;
	CenterAndExtent(mesh, center, size);
	size /= 2;

	FBox bounds = BoundingBox(mesh);
	Gm::TriangleMesh resultMeshX;
	Gm::TriangleMesh resultMeshY;
	Gm::TriangleMesh finalMesh;

	TArray<FColor> resultColorX;
	TArray<FColor> resultColorY;
	TArray<FColor> finalColor;
	
	for (size_t x = 0; x < Xnr; x++)
	{
		resultMeshX.Append(TransformGmMesh(FTransform(FRotator::ZeroRotator, FVector(bounds.GetSize().X * x, 0.0, 0.0), FVector(1.0, 1.0, 1.0)), mesh.GmMesh));
		resultColorX.Append(mesh.color);
	}

	for (size_t y = 0; y < Ynr; y++)
	{
		resultMeshY.Append(TransformGmMesh(FTransform(FRotator::ZeroRotator, FVector(  0.0, bounds.GetSize().Y * y, 0.0), FVector(1.0, 1.0, 1.0)), resultMeshX));
		resultColorY.Append(resultColorX);
	}

	for (size_t z = 0; z < Znr; z++)
	{
		finalMesh.Append(TransformGmMesh(FTransform(FRotator::ZeroRotator, FVector(0.0, 0.0, bounds.GetSize().Z * z), FVector(1.0, 1.0, 1.0)), resultMeshY));
		finalColor.Append(resultColorY);
	}

	outMesh.GmMesh = finalMesh;
	outMesh.color = finalColor;
	CopyGmTriangleMeshToBuffers(outMesh);
}

void AComposer_Scene::ApplyWave(int section, float amplitude, float frequency, float elapsedSeconds, float deltaSeconds, bool X, bool Y)
{
	TArray<FVector>* positions = RMC->BeginMeshSectionPositionUpdate(section);
	if (positions == nullptr)
		return;
	FBox box;
	
	ParallelFor(positions->Num(), [this, &positions, amplitude, frequency, elapsedSeconds, deltaSeconds, X, Y](int32 Index)
	{
		if(X)
			(*positions)[Index].X += amplitude * sin((*positions)[Index].Z*frequency / 1000 + elapsedSeconds) / (deltaSeconds*1000);
		if(Y)
			(*positions)[Index].Y += amplitude * cos((*positions)[Index].Z*frequency / 1000 + elapsedSeconds) / (deltaSeconds * 1000);
	});

	RMC->EndMeshSectionPositionUpdate(section);
}

void AComposer_Scene::Rotating(int section, FRotator rotation)
{
	TArray<FVector>* positions = RMC->BeginMeshSectionPositionUpdate(section);
	if (positions == nullptr)
		return;
	
	
	ParallelFor(positions->Num(), [this, &positions, rotation](int32 Index)
	{
		(*positions)[Index] = rotation.RotateVector( (*positions)[Index] );
	});
	RMC->EndMeshSectionPositionUpdate(section);
}

void AComposer_Scene::RotatingKeepN(int section, FRotator rotation)
{
	//TArray<FVector>* positions = RMC->BeginMeshSectionPositionUpdate(section);
	TArray<FVector>* positions;
	TArray<FRuntimeMeshVertexNoPosition>* vertices;
	RMC->BeginMeshSectionUpdate(section, positions, vertices);
	if (positions == nullptr || vertices == nullptr)
		return;

	ParallelFor(positions->Num(), [this, &positions, rotation](int32 Index)
	{
		(*positions)[Index] = rotation.RotateVector((*positions)[Index]);
	});

	ParallelFor(vertices->Num(), [this, &vertices, rotation](int32 Index)
	{
		(*vertices)[Index].Normal = rotation.RotateVector( (*vertices)[Index].Normal );
		(*vertices)[Index].Tangent = rotation.RotateVector((*vertices)[Index].Tangent);
	});

	RMC->UpdateMeshSection(section, *positions, *vertices);
}

void AComposer_Scene::Sectioned(int sectionId, bool createCollision, FMeshData meshData)
{
	TArray<FRuntimeMeshVertexNoPosition> vertices = CreateNoPositionVerticesFromBuffers(meshData.normals, meshData.tangents, meshData.uVs, meshData.color);
	RMC->CreateMeshSectionDualBuffer(sectionId, meshData.positions, vertices, meshData.indices, createCollision, EUpdateFrequency::Frequent);
}

//----------------------
// GM GEOMETRY FUNCTIONS
//----------------------
void AComposer_Scene::Cuboid(const FVector diagonal, const FVector segments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::CuboidDescriptor description;
	description.alternateGrid = false;
	description.size = { diagonal.X, diagonal.Y, diagonal.Z };
	description.segments = { (uint32)segments.X, (uint32)segments.Y, (uint32)segments.Z };

	Gm::TriangleMesh mesh = Gm::MeshGenerator::GenerateCuboid(description);
	if (winding) 
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);
	
	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::Ellipsoid(FVector radius, FVector2D segments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::EllipsoidDescriptor description;
	description.alternateGrid = true;
	description.radius = { radius.X, radius.Y, radius.Z };
	description.segments = { (uint32)segments.X, (uint32)segments.Y };

	Gm::TriangleMesh mesh = Gm::MeshGenerator::GenerateEllipsoid(description);
	if (winding)
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);

	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::Cylinder(const FVector2D radius, const float height, const FVector2D mantleSegments, const int topCoverSegments, const int bottomCoverSegments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::CylinderDescriptor description;
	description.alternateGrid = true;
	description.radius = { radius.X, radius.Y };
	description.height = height;
	description.mantleSegments = { (uint32)mantleSegments.X, (uint32)mantleSegments.Y };
	description.topCoverSegments = topCoverSegments;
	description.bottomCoverSegments = bottomCoverSegments;

	Gm::TriangleMesh mesh = Gm::MeshGenerator::GenerateCylinder(description);
	if (winding)
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);

	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::Pipe(const FTransform transform, const FVector2D innerRadius, const FVector2D outerRadius, const float height, const FVector2D mantleSegments, const int topCoverSegments, const int bottomCoverSegments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::PipeDescriptor description;
	description.alternateGrid = true;
	description.innerRadius = { innerRadius.X, innerRadius.Y };
	description.outerRadius = { outerRadius.X, outerRadius.Y };
	description.height = height;
	description.mantleSegments = { (uint32)mantleSegments.X, (uint32)mantleSegments.Y };
	description.topCoverSegments = topCoverSegments;
	description.bottomCoverSegments = bottomCoverSegments;


	Gm::TriangleMesh mesh = Gm::MeshGenerator::GeneratePipe(description);
	if (winding)
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);

	//Rotate and translate accordingly
	for (auto&v : mesh.vertices)
	{
		v.position = Gs::RotateVectorAroundAxis(v.position, { 1.0,0.0,0.0 }, 1.57f);
		v.normal = Gs::RotateVectorAroundAxis(v.normal, { 1.0,0.0,0.0 }, 1.57f);
		v.normal.Normalize();
	}
	mesh = TransformGmMesh(transform, mesh);

	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::Capsule(const FVector radius, const float height, const int32 ellipsoidSegments, const FVector2D mantleSegments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::CapsuleDescriptor description;
	description.alternateGrid = true;
	description.ellipsoidSegments = ellipsoidSegments;
	description.height = height;
	description.mantleSegments = { (uint32)mantleSegments.X, (uint32)mantleSegments.Y };
	description.radius = { radius.X, radius.Y, radius.Z };

	Gm::TriangleMesh mesh = Gm::MeshGenerator::GenerateCapsule(description);
	if (winding)
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);

	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
	
}

void AComposer_Scene::Torus(const FVector2D ringRadius, const FVector tubeRadius, const FVector2D segments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::TorusDescriptor description;
	description.alternateGrid = true;
	description.ringRadius = { ringRadius.X, ringRadius.Y };
	description.tubeRadius = { tubeRadius.X, tubeRadius.Y, tubeRadius.Z };
	description.segments = { (uint32)segments.X, (uint32)segments.Y };

	Gm::TriangleMesh mesh = Gm::MeshGenerator::GenerateTorus(description);
	if (winding)
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);

	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
}

void AComposer_Scene::TorusKnot(const FVector ringRadius, const float tubeRadius, const float innerRadius, const float loops, const float turns, const FVector2D segments, bool winding, FMeshData& outMeshData)
{
	Gm::MeshGenerator::TorusKnotDescriptor description;
	description.alternateGrid = true;
	description.ringRadius = { ringRadius.X, ringRadius.Y, ringRadius.Z };
	description.tubeRadius = tubeRadius;
	description.innerRadius = innerRadius;
	description.segments = { (uint32)segments.X, (uint32)segments.Y };
	description.loops = loops;
	description.turns = turns;

	Gm::TriangleMesh mesh = Gm::MeshGenerator::GenerateTorusKnot(description);
	if (winding)
		FlipGmMeshWinding(mesh);
	else
		FlipMeshNormals(mesh);

	outMeshData.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(outMeshData);
}

//------------------
// PRIVATE FUNCTIONS
//------------------
void AComposer_Scene::CopyGmTriangleMeshToBuffers(FMeshData& outMeshData)
{
	outMeshData.indices.Empty();
	outMeshData.normals.Empty();
	outMeshData.positions.Empty();
	outMeshData.uVs.Empty();
	outMeshData.tangents.Empty();
	
	//Copy Vertices
	for (size_t i = 0; i < outMeshData.GmMesh.vertices.size(); i++)
	{
		outMeshData.positions.Add(
			FVector
			(
				outMeshData.GmMesh.vertices[i].position.x,
				outMeshData.GmMesh.vertices[i].position.y,
				outMeshData.GmMesh.vertices[i].position.z
			));

			outMeshData.normals.Add(FVector
			(
				outMeshData.GmMesh.vertices[i].normal.x,
				outMeshData.GmMesh.vertices[i].normal.y,
				outMeshData.GmMesh.vertices[i].normal.z
			));
		
		outMeshData.uVs.Add(FVector2D(outMeshData.GmMesh.vertices[i].texCoord.x, outMeshData.GmMesh.vertices[i].texCoord.y));
	}

	//Copy Indices
	Gs::Vector3 tangent, bitangent, normal;
	
	outMeshData.tangents.Reserve(outMeshData.GmMesh.vertices.size());
	for (size_t i = 0; i < outMeshData.GmMesh.vertices.size(); i++)
		outMeshData.tangents.Add(FVector());

	for (size_t i = 0; i < outMeshData.GmMesh.triangles.size(); i++)
	{
		//Copy triangles
		outMeshData.indices.Add(outMeshData.GmMesh.triangles[i].a);
		outMeshData.indices.Add(outMeshData.GmMesh.triangles[i].b);
		outMeshData.indices.Add(outMeshData.GmMesh.triangles[i].c);

		//Compute tangent space per triangle
		Gm::Triangle3 coords(
			outMeshData.GmMesh.vertices[outMeshData.GmMesh.triangles[i].a].position,
			outMeshData.GmMesh.vertices[outMeshData.GmMesh.triangles[i].b].position,
			outMeshData.GmMesh.vertices[outMeshData.GmMesh.triangles[i].c].position

		);

		Gm::Triangle2 texCoords(
			outMeshData.GmMesh.vertices[outMeshData.GmMesh.triangles[i].a].texCoord,
			outMeshData.GmMesh.vertices[outMeshData.GmMesh.triangles[i].b].texCoord,
			outMeshData.GmMesh.vertices[outMeshData.GmMesh.triangles[i].c].texCoord
		);
		Gm::ComputeTangentSpace(coords, texCoords, tangent, bitangent, normal);
		FVector fTangent = GsVector3ToFVector(tangent);
		outMeshData.tangents[outMeshData.GmMesh.triangles[i].a] = fTangent;
		outMeshData.tangents[outMeshData.GmMesh.triangles[i].b] = fTangent;
		outMeshData.tangents[outMeshData.GmMesh.triangles[i].c] = fTangent;
	}

	if (outMeshData.color.Num() < outMeshData.GmMesh.vertices.size())
	{
		AssignBaseColorToMesh(outMeshData);
	}
}

Gm::TriangleMesh AComposer_Scene::TransformGmMesh(const FTransform transform, const Gm::TriangleMesh mesh)
{
	FVector translation = transform.GetLocation();
	FRotator rotation = transform.GetRotation().Rotator();
	FVector scale = transform.GetScale3D();
	Gm::TriangleMesh resultMesh = mesh;
	for (auto&v : resultMesh.vertices)
	{
		v.position = Gs::RotateVectorAroundAxis(v.position, { 1.0,0.0,0.0 }, rotation.Roll * (3.14f / 180.0f));
		v.position = Gs::RotateVectorAroundAxis(v.position, { 0.0,1.0,0.0 }, rotation.Pitch * (3.14f / 180.0f));
		v.position = Gs::RotateVectorAroundAxis(v.position, { 0.0,0.0,1.0 }, rotation.Yaw * (3.14f / 180.0f));
		
		v.position.x += translation.X;
		v.position.y += translation.Y;
		v.position.z += translation.Z;

		v.normal = Gs::RotateVectorAroundAxis(v.normal, { 1.0,0.0,0.0 }, rotation.Roll * (3.14f / 180.0f));
		v.normal = Gs::RotateVectorAroundAxis(v.normal, { 0.0,1.0,0.0 }, rotation.Pitch * (3.14f / 180.0f));
		v.normal = Gs::RotateVectorAroundAxis(v.normal, { 0.0,0.0,1.0 }, rotation.Yaw * (3.14f / 180.0f));
		v.normal.Normalize();
	}
	
	return resultMesh;
}

void AComposer_Scene::FlipGmMeshWinding(Gm::TriangleMesh & mesh)
{
	for (int i = 0; i < mesh.triangles.size(); i++)
	{
		auto tA = mesh.triangles[i].a;
		mesh.triangles[i].a = mesh.triangles[i].c;
		mesh.triangles[i].c = tA;
	}

	
}

TArray<FRuntimeMeshVertexNoPosition> AComposer_Scene::CreateNoPositionVerticesFromBuffers(const TArray<FVector> normals, TArray<FVector> tangents, const TArray<FVector2D> uv, TArray<FColor> color)
{
	TArray<FRuntimeMeshVertexNoPosition> result;
	for (size_t i = 0; i < normals.Num(); i++)
	{
		FRuntimeMeshVertexNoPosition vertex;
		vertex.Color = color[i];
		vertex.SetNormalAndTangent(normals[i], tangents[i]);
		//vertex.Normal = normals[i];
		vertex.UV0 = uv[i];
		result.Add(vertex);
	}
	return result;
}

FVector AComposer_Scene::GsVector3ToFVector(Gs::Vector3 vector)
{
	return FVector(vector.x, vector.y, vector.z);
}

void AComposer_Scene::AssignBaseColorToMesh(FMeshData & mesh)
{
	mesh.color.Empty();
	for (size_t i = 0; i < mesh.positions.Num(); i++)
	{
		mesh.color.Add(FColor(246, 0.0, 255, 1.0));
	}
}

FMeshData AComposer_Scene::ConvertToGMMesh(TArray<FVector> positions, TArray<FRuntimeMeshVertexNoPosition> vertices, TArray<int32> triangles)
{
	FMeshData result;
	Gm::TriangleMesh mesh;
	for (size_t i = 0; i < vertices.Num(); i++)
	{
		mesh.AddVertex(
			Gs::Vector3(positions[i].X,positions[i].Y, positions[i].Z),
			Gs::Vector3(vertices[i].Normal.Vector.X, vertices[i].Normal.Vector.Y , vertices[i].Normal.Vector.Z ),
			Gs::Vector2(vertices[i].UV0.X, vertices[i].UV0.Y )
		);
	}
	for (size_t i = 0; i < triangles.Num()-4; i+=3)
	{
		mesh.AddTriangle(i, i+1, i+2);
	}

	result.GmMesh = mesh;
	CopyGmTriangleMeshToBuffers(result);
	return result;
}

void AComposer_Scene::FlipMeshNormals(Gm::TriangleMesh & mesh)
{
	for (int i = 0; i < mesh.vertices.size(); i++)
	{
		mesh.vertices[i].normal = -mesh.vertices[i].normal;
	}
}
