// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "Gauss/Gauss.h"
#include "Geom/Geom.h"
#include <random>
#include "Runtime/Engine/Classes/Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Composer_Scene.generated.h"

USTRUCT(BlueprintType)
struct FMeshData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FVector> positions;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FVector> normals;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FVector> tangents;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FVector2D> uVs;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<int32> indices;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FColor> color;

	Gm::TriangleMesh GmMesh;

	bool winding = false;
};

UENUM(BlueprintType)
enum class EFacingEnum : uint8
{
	FE_Left 	UMETA(DisplayName = "Left"),
	FE_Right 	UMETA(DisplayName = "Right"),
	FE_Top		UMETA(DisplayName = "Top"),
	FE_Bottom	UMETA(DisplayName = "Bottom"),
	FE_Front	UMETA(DisplayName = "Front"),
	FE_Back		UMETA(DisplayName = "Back")
};

UENUM(BlueprintType)
enum class EAxisEnum : uint8
{
	AE_X 	UMETA(DisplayName = "X"),
	AE_Y 	UMETA(DisplayName = "Y"),
	AE_Z	UMETA(DisplayName = "Z")
};

UCLASS()
class PARAMETRICART18_API AComposer_Scene : public AActor
{
	GENERATED_BODY()

  public:
	// Sets default values for this actor's properties
	AComposer_Scene();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	URuntimeMeshComponent* RMC;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UHierarchicalInstancedStaticMeshComponent* HISMC;

  protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void CopyGmMeshToPoint(FTransform transform, Gm::TriangleMesh inMesh, Gm::TriangleMesh& outMesh);
	void CopyGmTriangleMeshToBuffers(FMeshData& outMeshData);
	Gm::TriangleMesh TransformGmMesh(const FTransform transform, const Gm::TriangleMesh mesh);
	void FlipGmMeshWinding(Gm::TriangleMesh& mesh);
	TArray<FRuntimeMeshVertexNoPosition> CreateNoPositionVerticesFromBuffers(const TArray<FVector> normals, TArray<FVector> tangents, const TArray<FVector2D> uv, TArray<FColor> color);
	FVector GsVector3ToFVector(Gs::Vector3 vector);
	void AssignBaseColorToMesh(FMeshData& mesh);
	FMeshData ConvertToGMMesh(TArray<FVector> positions, TArray<FRuntimeMeshVertexNoPosition> vertices, TArray<int32> triangles);
	void FlipMeshNormals(Gm::TriangleMesh& mesh);

  public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Circle(float resolution, float r, FVector center);

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Ray(float resolution, FVector origin, FVector direction);

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Wave(FVector direction, bool X, bool Y, bool Z, float resolution, float amplitude, float frequency);

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Square(float resolution, FVector diagonal);

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Spiral(float resolution, float radius, float height, int revolutions);

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Rotated(TArray<FVector> Points, FRotator rotation);

	UFUNCTION(BlueprintCallable, Category = "Composer-Points")
	TArray<FVector> Noise(const FVector point, const int count, const float width, const float length, const float height);

	UFUNCTION(BlueprintCallable, Category = "Composer-Section")
	FBox SectionBoundingBox(int section);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	FBox BoundingBox(FMeshData mesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void CenterAndExtent(FMeshData mesh, FVector& center, FVector& extent);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Clipped(FMeshData mesh, FVector p1, FVector p2, FVector p3, bool winding, FMeshData& front, FMeshData& back);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Transformed(FTransform transform, FMeshData mesh, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	FMeshData MakeMeshData(TArray<FVector> vertices, TArray<FVector> normals, TArray<FVector2D> texturecoordinates, TArray<int> triangles);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Staple(FMeshData mesh, FMeshData staple, bool left, bool right, bool top, bool bottom, bool front, bool back, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Section")
	void SectionData(int index, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Section")
	void TriangleCount(int index, int& count);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Snap(FMeshData target, FMeshData mesh, EFacingEnum facing, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Colored(FMeshData mesh, FColor color, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Mirrored(FMeshData mesh, FVector point, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Centered(FMeshData mesh, FVector point, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Instantiate")
	void Instantiated (TArray<FVector> points, FTransform transform, bool make_XRotator, bool make_YRotator, bool make_ZRotator);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Copied(FMeshData mesh, TArray<FVector> points, bool make_XRotator, bool make_YRotator, bool make_ZRotator, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Merged(FMeshData meshData_1, FMeshData meshData_2, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Section")
	void Repeated_Sec(int section, int Xnr, int Ynr, int Znr);
	
	UFUNCTION(BlueprintCallable, Category = "Composer-Mesh")
	void Repeated(FMeshData mesh, int Xnr, int Ynr, int Znr, FMeshData& outMesh);

	UFUNCTION(BlueprintCallable, Category = "Composer-Runtime")
	void ApplyWave(int section, float amplitude, float frequency, float elapsedSeconds, float deltaSeconds, bool X, bool Y);

	UFUNCTION(BlueprintCallable, Category = "Composer-Runtime")
	void Rotating(int section, FRotator rotation);

	UFUNCTION(BlueprintCallable, Category = "Composer-Runtime")
	void RotatingKeepN(int section, FRotator rotation);

	UFUNCTION(BlueprintCallable, Category = "Composer-Section")
	void Sectioned(int sectionId, bool createCollision, FMeshData meshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void Cuboid(const FVector diagonal, const FVector segments, bool winding, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void Ellipsoid(FVector radius, FVector2D segments, bool winding, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void Cylinder(const FVector2D radius, const float height, const FVector2D mantleSegments, const int topCoverSegments, const int bottomCoverSegments, bool winding, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void Pipe(const FTransform transform, const FVector2D innerRadius, const FVector2D outerRadius, const float height, const FVector2D mantleSegments, const int topCoverSegments, const int bottomCoverSegments, bool winding, FMeshData& outMeshData);
	
	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void Capsule(const FVector radius, const float height, const int32 ellipsoidSegments, const FVector2D mantleSegments, bool winding, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void Torus(const FVector2D ringRadius, const FVector tubeRadius, const FVector2D segments, bool winding, FMeshData& outMeshData);

	UFUNCTION(BlueprintCallable, Category = "Composer-Geometry")
	void TorusKnot(const FVector ringRadius, const float tubeRadius, const float innerRadius, const float loops, const float turns, const FVector2D segments, bool winding, FMeshData& outMeshData);
};